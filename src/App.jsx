import React, { Component, useState } from "react";
import SearchBar from "./components/SearchBar";
import SubmitButton from "./components/SubmitButton";
import Card from "./components/Card";
import MiniCard from "./components/MiniCard";
import "./App.css";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [dayData, setDayData] = useState(null);
  const [futureData, setFutureData] = useState([]);

  const handleSearch = (event) => {
    setInputValue(event.target.value);
  };
  const convertToJson = (data) => {
    const jsonData = data.json();
    return Promise.resolve(jsonData);
  };
  const displayJSON = (data) => {
    console.log("Expected data :", data);
    return Promise.resolve();
  };

  function toCelcius(data) {
    return Math.floor(data - 273.15)
      .toString()
      .concat("°C");
  }

  const setDisplayData = (jsonData) => {
    const weatherObject = jsonData.list["0"].main;

    setDayData({
      name: jsonData.city.name,
      temp: toCelcius(weatherObject.temp),
      feels_like: toCelcius(weatherObject.feels_like),
      icon: jsonData.list["0"].weather[0].icon,
      date: jsonData.list["0"]["dt_txt"],
    });

    let indexes = ["8", "16", "24", "32"];

    indexes.forEach((index) => {
      setFutureData((futureData) => [
        ...futureData,
        {
          temp: toCelcius(jsonData.list[index].main.temp),
          feels_like: toCelcius(jsonData.list[index].main.feels_like),
          icon: jsonData.list[index].weather[0].icon,
          date: jsonData.list[index]["dt_txt"],
        },
      ]);
    });

    return Promise.resolve();
  };

  const handleSubmit = (event) => {
    if (inputValue === "") {
      setDayData("default");
      setFutureData([])
      return;
    }
    try {
      fetch(
        `https://api.openweathermap.org/data/2.5/forecast?q=${inputValue}&appid=f5f8097a936ff6e70060ecb1ada6f932`
      )
        .then((response) => convertToJson(response))
        .then((jsonData) => setDisplayData(jsonData))
        // .then(jsonData=>displayJSON(jsonData))
        .catch((err) => console.error(err));

      if (dayData === "default") {
        setDayData("Loading");
      }
    } catch (err) {
      console.log(err.message);
    }
  };

  return (
    <>
      <SearchBar handleSearch={handleSearch} />
      <SubmitButton handleSubmit={handleSubmit} />
      <Card data={dayData ? dayData : "default"} />
      <ul>{futureData!=[]? (
        futureData.map((future) => {
          return <li key={inputValue.concat(future.date)}><MiniCard  data={future} /></li>;
        })
      ) : (
        <></>
      )}</ul>
    </>
  );
}

export default App;
