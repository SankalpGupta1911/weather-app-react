import React, { Component } from 'react';

function SearchBar(props) {
  return ( 
    <input className='.search-bar' type="text" name="search" id="search" onChange={props.handleSearch} placeholder='Search for a city...'/>
   );
}

export default SearchBar;