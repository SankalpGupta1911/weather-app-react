import React, { Component } from "react";

function Card(props) {
  
  const checkAndLoad = (props) => {
    if (props.data === "default") {
      return <h1>Please enter a city name and click submit</h1>;
    } else if (props.data === "Loading") {
      return <h1>Loading Data...</h1>;
    } else {
      let link = `https://openweathermap.org/img/wn/${props.data.icon}@2x.png`
      let icon = <img id="icon" src={"".concat(link)} alt="" />
      return (
        <>
          <h1>{props.data.name}</h1>
          <div id="date-time"><h2>{props.data.date}</h2></div>
          <main>
            <div className="left-panel">
              <div className="temp">
                <img
                  id="temp-icon"
                  src="https://img.freepik.com/premium-vector/thermometers-measuring-heat-cold-vector-illustration-icon-thermometer-equipment-showing-hot-c_144920-612.jpg?w=740"
                  alt="image"
                />
                <h2>{props.data.temp}</h2>
              </div>
              <div className="temp">
                <img
                  id="temp-icon"
                  src="https://img.freepik.com/premium-vector/thermometers-measuring-heat-cold-vector-illustration-icon-thermometer-equipment-showing-hot-c_144920-612.jpg?w=740"
                  alt="image"
                />
                <h3>Feels like: {props.data.feels_like}</h3>
              </div>
            </div>
            <div className="right-panel">
              {icon}
            </div>
          </main>
        </>
      );
    }
  };

  return <div className="card">{checkAndLoad(props)}</div>;
}

export default Card;
