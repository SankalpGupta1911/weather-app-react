import React, { Component } from 'react';

function SubmitButton(props) {
    return ( 
        <input type="submit" value="Submit" onClick={props.handleSubmit}/>
     );
}

export default SubmitButton;